@rem (c) https://github.com/MontiCore/monticore  

@echo off




set jar= %~dp0%ocl-1.0.0-SNAPSHOT-jar-with-dependencies.jar
set parent_dir= "%~dp0\"
set ocl= "ocl.exampleNonValidType"


cd %JDK_PATH%

java -jar %JAR% -path %parent_dir% -ocl %ocl%


pause

