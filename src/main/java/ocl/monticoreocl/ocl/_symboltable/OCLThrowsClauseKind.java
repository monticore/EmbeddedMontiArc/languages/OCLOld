/* (c) https://github.com/MontiCore/monticore */
package ocl.monticoreocl.ocl._symboltable;

import de.monticore.symboltable.SymbolKind;

public class OCLThrowsClauseKind implements SymbolKind {

	public static final OCLThrowsClauseKind INSTANCE = new OCLThrowsClauseKind();

	protected OCLThrowsClauseKind() {
	}
}
