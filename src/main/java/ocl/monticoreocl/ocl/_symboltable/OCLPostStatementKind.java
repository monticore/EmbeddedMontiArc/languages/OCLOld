/* (c) https://github.com/MontiCore/monticore */
package ocl.monticoreocl.ocl._symboltable;

import de.monticore.symboltable.SymbolKind;

public class OCLPostStatementKind implements SymbolKind {

	public static final OCLPostStatementKind INSTANCE = new OCLPostStatementKind();

	protected OCLPostStatementKind() {
	}
}
