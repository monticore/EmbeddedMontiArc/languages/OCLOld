/* (c) https://github.com/MontiCore/monticore */
package ocl.monticoreocl.ocl._symboltable;

import de.monticore.symboltable.SymbolKind;

public class OCLFileSymbolKind implements SymbolKind {

	public static final OCLFileSymbolKind INSTANCE = new OCLFileSymbolKind();

	protected OCLFileSymbolKind() {
	}
}
