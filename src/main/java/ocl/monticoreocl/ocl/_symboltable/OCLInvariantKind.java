/* (c) https://github.com/MontiCore/monticore */
package ocl.monticoreocl.ocl._symboltable;

import de.monticore.symboltable.SymbolKind;

public class OCLInvariantKind implements SymbolKind {

	public static final OCLInvariantKind INSTANCE = new OCLInvariantKind();

	protected OCLInvariantKind() {
	}
}
