/* (c) https://github.com/MontiCore/monticore */
package ocl.monticoreocl.ocl._symboltable;

import de.monticore.symboltable.SymbolKind;

public class OCLConstructorSignatureKind implements SymbolKind {

	public static final OCLConstructorSignatureKind INSTANCE = new OCLConstructorSignatureKind();

	protected OCLConstructorSignatureKind() {
	}
}
