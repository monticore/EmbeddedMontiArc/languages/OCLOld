/* (c) https://github.com/MontiCore/monticore */
package ocl.monticoreocl.ocl._symboltable;

import de.monticore.symboltable.SymbolKind;

public class OCLPreStatementKind implements SymbolKind {

	public static final OCLPreStatementKind INSTANCE = new OCLPreStatementKind();

	protected OCLPreStatementKind() {
	}
}
