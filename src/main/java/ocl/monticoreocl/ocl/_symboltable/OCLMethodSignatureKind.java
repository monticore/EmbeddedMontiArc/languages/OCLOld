/* (c) https://github.com/MontiCore/monticore */
package ocl.monticoreocl.ocl._symboltable;

import de.monticore.symboltable.SymbolKind;

public class OCLMethodSignatureKind implements SymbolKind {

	public static final OCLMethodSignatureKind INSTANCE = new OCLMethodSignatureKind();

	protected OCLMethodSignatureKind() {
	}
}
