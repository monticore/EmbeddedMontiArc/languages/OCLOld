/* (c) https://github.com/MontiCore/monticore */
package ocl.monticoreocl.ocl._symboltable;

import de.monticore.symboltable.SymbolKind;

public class OCLParameterDeclarationKind implements SymbolKind {

	public static final OCLParameterDeclarationKind INSTANCE = new OCLParameterDeclarationKind();

	protected OCLParameterDeclarationKind() {
	}
}
