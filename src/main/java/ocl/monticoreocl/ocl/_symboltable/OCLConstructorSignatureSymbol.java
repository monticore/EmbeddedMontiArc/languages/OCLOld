/* (c) https://github.com/MontiCore/monticore */
package ocl.monticoreocl.ocl._symboltable;

import static de.monticore.symboltable.Symbols.sortSymbolsByPosition;

import java.util.Collection;
import java.util.Optional;

import de.monticore.symboltable.CommonScopeSpanningSymbol;

public class OCLConstructorSignatureSymbol extends CommonScopeSpanningSymbol {

	public static final OCLConstructorSignatureKind KIND = OCLConstructorSignatureKind.INSTANCE;

	public OCLConstructorSignatureSymbol(String name) {
		super(name, KIND);
	}

	public Collection<OCLParameterDeclarationSymbol> getOCLParamDecl() {
		return sortSymbolsByPosition(getSpannedScope().resolveLocally(OCLParameterDeclarationSymbol.KIND));
	}

	public Optional<OCLParameterDeclarationSymbol> getOCLParamDecl(String name) {
		return getSpannedScope().resolve(name, OCLParameterDeclarationSymbol.KIND);
	} 

	public Collection<OCLThrowsClauseSymbol> getOCLThrowsClause() {
		return sortSymbolsByPosition(getSpannedScope().resolveLocally(OCLThrowsClauseSymbol.KIND));
	}
}
