/* (c) https://github.com/MontiCore/monticore */
package ocl.monticoreocl.ocl._symboltable;

import de.monticore.symboltable.SymbolKind;

public class OCLMethodDeclarationKind implements SymbolKind {

	public static final OCLMethodDeclarationKind INSTANCE = new OCLMethodDeclarationKind();

	protected OCLMethodDeclarationKind() {
	}
}
