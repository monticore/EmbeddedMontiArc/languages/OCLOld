/* (c) https://github.com/MontiCore/monticore */
package ocl.monticoreocl.ocl._visitors;

import de.monticore.symboltable.MutableScope;
import de.monticore.symboltable.Scope;
import de.monticore.umlcd4a.symboltable.references.CDTypeSymbolReference;
import de.se_rwth.commons.logging.Log;
import ocl.monticoreocl.ocl._ast.*;
import ocl.monticoreocl.ocl._visitor.OCLVisitor;


public class OCLTypeCheckingVisitor implements OCLVisitor{

    private boolean isTypeCorrect;
    private OCLVisitor realThis = this;
    private MutableScope scope;

    public OCLTypeCheckingVisitor(MutableScope scope) {
        this.isTypeCorrect = true;
        this.scope = scope;
    }

    public boolean isTypeCorrect() {
        return isTypeCorrect;
    }

    public static void checkInvariants(ASTOCLInvariant node, MutableScope scope) {
        OCLTypeCheckingVisitor checkingVisitor = new OCLTypeCheckingVisitor(scope);

        for(ASTOCLExpression expr : node.getStatements()){
            expr.accept(checkingVisitor);
            if(!checkingVisitor.isTypeCorrect()) {
                Log.warn("Something went wrong in this Invariant", expr.get_SourcePositionStart());
            }
        }
    }

    @Override
    public void traverse(ASTOCLIsin node){
        Log.warn("Todo: implement type checking for isIn nodes.");
    }

    @Override
    public void traverse(ASTOCLComprehensionPrimary node){
        Log.warn("Todo: implement type checking for comprehensions.");
    }

    @Override
    public void traverse(ASTOCLParenthizedExpr node){
        OCLExpressionTypeInferingVisitor.getTypeFromExpression(node, scope);
    }

    @Override
    public void traverse(ASTOCLConcatenation node){
        OCLExpressionTypeInferingVisitor.getTypeFromExpression(node, scope);
    }

    @Override
    public void traverse(ASTOCLQualifiedPrimary node){
        OCLExpressionTypeInferingVisitor.getTypeFromExpression(node, scope);
    }
}
